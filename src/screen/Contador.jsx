import React, {useState, useEffect} from "react";

const Contador = () => {

    const [count, setCount] = useState(15) ;
    useEffect(
      () =>{
        //to do
        console.log("TOTAL: " + count);
      }, [count]
    );

    return (
        <div>
             <h1>hola tailwind h1</h1>
      <hr/>
          la cuenta es {count}
          <hr/>
          <button
            className='py-2 px-4 font-semibold rounded-lg shadow-md text-white bg-green-500 hover:bg-green-700 '
            onClick={ () => setCount(count + 1) } > Aumentar </button>
        </div>
    )
}

export default Contador
