import React, {useState, useEffect} from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink
} from "react-router-dom";

import About from './screen/About.jsx';
import Contador from "./screen/Contador.jsx";

export default function App() {

  return (
    <>
    <Router>
      <NavLink to="/about" className="py-2 px-4 font-semibold rounded-lg 
                                  shadow-md text-white bg-blue-500 
                                  hover:bg-blue-700"
                                  activeClassName="bg-blue-800"    
                                  >About</NavLink>
                                  
      <NavLink  to="/contador" className="py-2 px-4 font-semibold rounded-lg 
                                      shadow-md text-white bg-red-500 
                                      hover:bg-red-700"
                                      activeClassName="bg-red-800"       
                                      >Contador</NavLink >
      <Switch>
        <Route path="/about" exact>
          <About />
        </Route>
        <Route path="/contador" exact>
          <Contador />
        </Route>
      </Switch>
    </Router>

    </>
  );//return
}