import React from 'react'

export default function Apodo(props){
   
    let {name, age, color} = props.userInfo;
    let msj = `Hola ${name} `;

    if(color){
        msj += ` y tu color favorito es ${color}`;
    }

    if(age){
        msj += ` y tu edad  es ${age}`;
    }

    return (
        <div>
            <h2>
              {msj}
            </h2>
        </div>
    );
}

